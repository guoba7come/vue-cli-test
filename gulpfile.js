/**
 * 定义任务
 * 1. rm dist
 * 2. 上传dist upload
 */

const gulp = require("gulp");
const gulpSSH = require("gulp-ssh");

const filepath = "/usr/local/nginx/html/dist";
const command = `rm -rf ${filepath}`;
const gulpSsh = new gulpSSH({
  ignoreErrors: false,
  sshConfig: {
    host: "47.104.172.204",
    port: 22,
    username: "root",
    password: "Zhengyan369",
  },
});
// 登录远程服务器，删除 dist 文件
gulp.task("del", (done) => {
  // console.log(command);
  // gulpSsh.shell(["touch 1.txt"]);
  gulpSsh.shell([command]);
  done();
});

// 上传文件
gulp.task("upload", (done) => {
  gulp.src("./dist/**").pipe(gulpSsh.dest(filepath));
  done();
});
